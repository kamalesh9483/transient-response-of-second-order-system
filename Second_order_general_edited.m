clear;
clc;
close all;

% Receiving inputs
s = tf('s');
% w = input('Enter natural frequency in format of [w1,w2,w3] or [w1:stepsize:w2]');
w = 1;
OLTF = input('Enter OLTF general form for..(type w for natural frequency and for zeta\n');
R = input('Enter the input response\n');
z = input('Enter Damping ratio in format of [z1,z2,z3] or [z1:stepsize:z2]');
H = input('Enter feedback\n');
t = input('Enter time limits in format of [tmin,tmax]\n');

% for wn = w
%     for zeta = z
CLTF = feedback(OLTF,H);  
Cs = series(R,CLTF);  

[num,den] = tfdata(Cs);
syms x
n = poly2sym(num,x);
d = poly2sym(den,x);
Ct = ilaplace(n/d);

fplot(L,time);
hold all


%     end
% end
% Finding theta
theta = atan(sqrt(1-z^2)/z);

% Finding Rise time, tr
tr = (pi - theta)/(w*sqrt(1-z^2));

% Finding Peak time
Pt = pi/(w*sqrt(1-z^2));

% Finding Maximum Overshoot Mp, % Mp
Mp = exp((-z*pi)/sqrt(1-z^2));
per_Mp = 100 * Mp;

% Peak time, tp
tp = pi/(w*sqrt(1-z^2));

% Time constant
Tc = 1/ (z*w);

% Settling time for 5% error
ts_5 = 3*Tc;

% Settling time for 2% error
ts_2 = 4*Tc;

% Displaying Transient response of system
disp("The Time domain response is %f \n");
disp(Ct);
fprintf("The Rise time is %f \n",tr);
fprintf("The Peak time is %f \n",Pt);
fprintf("The Percentage Overshoot is %f \n",per_Mp);
fprintf("Settling time for 5% error %f",ts_5);
fprintf("The Settling time for 2% error %f",ts_2);

%     end
xlabel('Time in sec')